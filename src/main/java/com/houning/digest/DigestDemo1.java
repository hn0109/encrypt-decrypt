package com.houning.digest;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class DigestDemo1 {
    public static void main(String[] args) throws Exception {
        // 原文
        String input = "aa";
        // 算法
        String algorithm = "MD5";
        String md5 = getDigest(input, algorithm);
        System.out.println(md5);

        String sha1 = getDigest(input, "SHA-1");
        System.out.println(sha1);

        String sha256 = getDigest(input, "SHA-256");
        System.out.println(sha256);

        String sha512 = getDigest(input, "SHA-512");
        System.out.println(sha512);
    }

    /**
     * 获取数字摘要
     *
     * @param input     原文
     * @param algorithm 算法
     * @return
     * @throws Exception
     */
    private static String getDigest(String input, String algorithm) throws Exception {
        // 创建消息摘要对象
        MessageDigest digest = MessageDigest.getInstance(algorithm);
        // 执行消息摘要算法
        byte[] bytes = digest.digest(input.getBytes(StandardCharsets.UTF_8));
        return toHex(bytes);
    }

    private static String toHex(byte[] digest) {
        StringBuilder builder = new StringBuilder();
        // 对密文进行迭代
        for (byte b : digest) {
            // 把密文转换成16进制
            String s = Integer.toHexString(b & 0xff);
            // 如果密文的长度是1，需要在高位进行补0
            if (s.length() == 1) {
                builder.append('0');
            }
            builder.append(s);
        }
//        System.out.println(builder);
        return builder.toString();
    }
}
