package com.houning.digest;

import com.houning.rsa.RSADemo;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.util.Base64;

public class SignatureDemo {
    public static void main(String[] args) throws Exception {
        String a = "123";

        PublicKey publicKey = RSADemo.getPublicKey("a.pub", "RSA");
        PrivateKey privateKey = RSADemo.getPrivateKey("a.pri", "RSA");

        // 获取数字签名
        String signatureData = getSignature(a, "sha256withrsa", privateKey);
        System.out.println(signatureData);

        // 校验签名
        boolean b = verifySignature(a, "sha256withrsa", publicKey, signatureData);
        System.out.println(b);
    }

    /**
     * 校验签名
     * @param input 原文
     * @param algorithm 算法
     * @param publicKey 公钥
     * @param signatureData 签名密文
     * @return
     */
    private static boolean verifySignature(String input, String algorithm, PublicKey publicKey, String signatureData) throws Exception {
        // 获取签名对象
        Signature signature = Signature.getInstance(algorithm);
        // 初始化校验
        signature.initVerify(publicKey);
        // 传入原文
        signature.update(input.getBytes(StandardCharsets.UTF_8));
        byte[] decoded = Base64.getDecoder().decode(signatureData);
        return signature.verify(decoded);
    }

    /**
     * 生成数字签名
     *
     * @param input      原文
     * @param algorithm  算法
     * @param privateKey 私钥
     * @return
     */
    private static String getSignature(String input, String algorithm, PrivateKey privateKey) throws Exception {
        // 获取签名对象
        Signature signature = Signature.getInstance(algorithm);
        // 初始化签名
        signature.initSign(privateKey);
        // 传入原文
        signature.update(input.getBytes(StandardCharsets.UTF_8));
        // 开始签名
        byte[] sign = signature.sign();
        // 使用base64进行编码
        return Base64.getEncoder().encodeToString(sign);
    }
}
