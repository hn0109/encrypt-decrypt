package com.houning.desaes;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * 对称加密
 */
public class DesAesDemo {

    public static void main(String[] args) throws Exception {
        encryptDes();
    }

    /**
     * 解密
     *
     * @param encryptDes     密文
     * @param key            密钥
     * @param algorithm      加密算法
     * @param transformation 加密类型
     */
    private static String decryptDes(String encryptDes, String key, String algorithm, String transformation) throws Exception {
        Cipher cipher = Cipher.getInstance(algorithm);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), transformation);
        IvParameterSpec iv = new IvParameterSpec("12345678".getBytes(StandardCharsets.UTF_8));
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, iv);

        byte[] decoded = Base64.getDecoder().decode(encryptDes);

        // 解密，传入密文
        byte[] bytes = cipher.doFinal(decoded);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    private static void encryptDes() throws Exception {
        // 原文
        String input = "硅谷";

        // 定义key
        // 如果使用des进行加密，密钥必须是8个字节
        String key = "12345678";

        // 算法
        String algorithm = "DES/CBC/PKCS5Padding";

        // 加密类型
        String transformation = "DES";

        // 创建加密对象
        Cipher cipher = Cipher.getInstance(algorithm);

        // 创建加密规则
        // 第一个参数：表示key的字节
        // 第二个参数：表示加密的类型
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), transformation);
        IvParameterSpec iv = new IvParameterSpec("12345678".getBytes(StandardCharsets.UTF_8));
        // 进行加密初始化
        // 第一个参数表示模式，加密模式，解密模式
        // 第二个参数表示：加密的规则
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);

        // 调用加密方法
        // 参数表示原文的字节数组
        byte[] bytes = cipher.doFinal(input.getBytes(StandardCharsets.UTF_8));

        // 创建base64对象
        String encoded = Base64.getEncoder().encodeToString(bytes);
        System.out.println(encoded);

//        // 打印密文
//        // 如果直接打印密文会出现乱码
//        System.out.println(new String(bytes, StandardCharsets.UTF_8));
//        System.out.println(Arrays.toString(bytes));

        String inputText = decryptDes(encoded, key, algorithm, transformation);
        System.out.println(inputText);
    }
}
